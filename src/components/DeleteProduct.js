import axios from "axios";
import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import validator from "validator"
import "./UpdateProduct.css"

class DeleteProduct extends Component {

  constructor(props) {
    super(props)

    this.state = {
      isFormSubmitted: false,
      isLoading: false,
      isIdValid: ""
    }
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    if((!validator.isDivisibleBy(event.target.value, 1)) ||(Number(event.target.value) > 20) || (Number(event.target.value) < 1)) {
      this.setState({
        isIdValid: "Please Enter a Valid Id, Enter a number between 1 and 20"
      })}
       else {
        this.setState({
          isIdValid: "",
          id: event.target.value
        })
      }
  }

  handleForm = (event) => {
    event.preventDefault();  
    this.setState({
      isLoading: true
    })
    axios.delete(`https://fakestoreapi.com/products/${event.target[0].value}`,{
            data: ""
        })
        .then((resolve) => resolve.data)
        .then((data) => {
          this.props.deleteProduct(data)
          this.setState({
            isLoading: false,
            isFormSubmitted : true
            
          })
        })
        .catch((error) => {
          console.log(error);
        })
  }  
  render() {

    if(this.state.isLoading) {
      return(
        <>
        <div className="spinner-container">
          <div className="text-center">
            <div className="spinner-border" role="status">
              <span className="visually-hidden">Loading...</span>
            </div>
            <h4>Please wait, we are processing your request.</h4>
          </div>
        </div>
        </>
      )
    }

    if(this.state.isFormSubmitted) {
      return(
        <>
          <div className="product-update-successful-container">
            <h1>Product Deleted Successfully!</h1>
            <NavLink to="/Products" className="text-center">
              <button className="btn btn-primary w-50 h-100">Go To Products</button>
            </NavLink>
          </div>
        </>
      )
    } else {
      return (
        <>
        <div className="id-container">
          <h2>Enter Id of the Product to Delete</h2>
          <form onSubmit={this.handleForm}>
            <input
              type="text"
              style={{border: "1px solid black" }}
              onChange={this.handleChange}
              className="w-25 h-100"
            ></input>
            <p className="error-text">{this.state.isIdValid}</p>
            <div>
              <button
                className="btn btn-primary w-25 h-100"
                type="submit"
              >
                Delete
              </button>
            </div>
          </form>
        </div>
      </>
    );
    }
    
  }
}

export default DeleteProduct;
