import React, { Component } from "react";
import "./ProductsStyle.css";

class Product extends Component {
  constructor(props) {
    super(props);
    const {
      match: { params },
    } = this.props;
    const { id } = params;

    this.state = {
      myProduct: "",
      productId: id,
      isDataFetched: false,
      isDataLoading: true,
      errorState: false
    };
  }

  componentDidMount() {
    fetch(`https://fakestoreapi.com/products/${this.state.productId}`)
      .then((res) => res.json())
      .then((json) => {
        this.setState(
          {
            myProduct: json,
            isDataLoading: false,
            isDataFetched: true
          }
        );
      })
      .catch((error) => {
        console.log(error);
        this.setState({
          isDataLoading: false,
          errorState: true 
        })
      })
  }

  render() {
    if(this.state.isDataLoading) {
      return(
        <>
          <div className="spinner-container">
          <div className="text-center">
            <div className="spinner-border" role="status">
              <span className="visually-hidden">Loading...</span>
            </div>
          </div>
        </div>
        </>
      )
    }

    if (this.state.isDataFetched) {
      if(this.state.myProduct) {
        return (
          <div className="container products-main-container">
            <div className="row justify-content-around">
              <div className="col-12 col-sm-6 col-md-4 product-card-container">
                <div className="product-image-container">
                  <img className="image-size" src={this.state.myProduct.image} alt="" />
                </div>
                {/* Product heading and price container */}
                <div className="product-heading-price-container">
                  <h6 className="product-heading">{this.state.myProduct.title}</h6>
                </div>
  
                {/* Rating and pricing container */}
                <div className="product-rating-price-container">
                  <div>
                    <p className="rating-para">{`Rating: ${this.state.myProduct.rating.rate}`}</p>
                  </div>
                  <div className="price-container">
                    <i className="fa-solid fa-indian-rupee-sign price-icon"></i>
                    <p className="price-para ">{this.state.myProduct.price}</p>
                  </div>
                </div>
                <div>
                  <p className="category-para">{`Category: ${this.state.myProduct.category}`}</p>
                  <p className="description-para">{this.state.myProduct.description}</p>
                </div>
              </div>
            </div>
          </div>
        );
      } else {
        return (
          <>
            <div className="api-unsuccessful">
              <h1>Oops! No Products Found. Please Try Again After Some Time.</h1>
            </div>
          </>
        );
      }
      
    }  else if (this.state.errorState) {
      return (
        <>
          <div className="api-unsuccessful">
            <h1>404 Not Found</h1>
            <h4>
              The Page you are requesting is not found. Please try again after
              some time.
            </h4>
          </div>
        </>
      );
    } 
  }
}


export default Product;
