import React from "react";
import { NavLink } from "react-router-dom";
import "./HeaderStyles.css";

function Header() {
  return (
    <>
      <nav className="navbar navbar-expand-md navbar-light bg-light nav-position">
        <div className="container-fluid justify-content-between">
          <h1 className="logo-heading">WEST SIDE</h1>
          <hr/>
          <button
            className="navbar-toggler "
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbarNav"
            aria-controls="navbarNav"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse justify-content-end" id="navbarNav">
            <ul className="navbar-nav">
              <li className="nav-item">
                <NavLink to="/">Home</NavLink>
              </li>
              <li className="nav-item">
                <NavLink to="/About">About</NavLink>
              </li>
              <li className="nav-item">
                <NavLink to="/Products">Products</NavLink>
              </li>
              <li className="nav-item">
                <NavLink to="/SignUp">Signup</NavLink>
              </li>
              <li className="nav-item">
                <a href="/CreateProduct">CreateProduct</a>
              </li>
              <li className="nav-item">
                <a href="/UpdateProduct">UpdateProduct</a>
              </li>
              <li className="nav-item">
                <a href="/DeleteProduct">DeleteProduct</a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    </>
  );
}

export default Header;
