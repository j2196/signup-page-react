import React from "react";
import "./PageStyles.css";

function Home() {
  return (
    <>
      <section>
        <div className="container page-main-container">
          <div className="row">
            <div className="col-12  text-white p-4">
              <h1 className="section-heading">We work Efficiently</h1>
              <p className="section-paragraph">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                Suspendisse quis tortor varius, blandit mauris non, semper
                lacus. Cras vel sem bibendum, congue nulla bibendum, malesuada
                lorem. Duis pharetra efficitur cursus. Nunc quis nibh vel eros
                placerat facilisis sed non velit. Sed quis elit sodales,
                fringilla enim quis, hendrerit est. Maecenas convallis congue
                metus, vitae faucibus nunc laoreet sed. Vestibulum ante ipsum
                primis in faucibus orci luctus et ultrices posuere cubilia
                curae;
              </p>
              <p className="section-paragraph">
                Nulla tristique, turpis in sodales elementum, nulla tortor
                ullamcorper metus, eu semper turpis justo vel risus. Integer vel
                nisl nisi. Cras id erat ut eros commodo suscipit sit amet ut
                turpis. Donec lacus erat, luctus id tristique non, mollis vel
                sem. Aliquam ut lobortis nisi, ac viverra nisi. Aliquam ac erat
                eu turpis venenatis lacinia. Vivamus facilisis, ante at finibus
                sodales, metus neque cursus massa, id semper urna diam id nunc.
                Curabitur a sollicitudin libero, non volutpat dolor. Nullam
                euismod, magna vel bibendum porttitor, nibh ligula pellentesque
                metus, at ornare nulla odio sed nibh. Aenean lacinia lectus eget
                massa laoreet, sed pretium eros posuere. Maecenas viverra
                vehicula sapien a porta. Quisque sollicitudin luctus velit eu
                feugiat. Pellentesque habitant morbi tristique senectus et netus
                et malesuada fames ac turpis egestas.
              </p>
            </div>
          </div>
        </div>
      </section>
    </>
  );
}

export default Home;
