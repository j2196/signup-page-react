import axios from "axios";
import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import "./ProductsStyle.css";

class Products extends Component {
  constructor(props) {
    super(props);

    this.state = {
      myData: "",
      isFetched: false,
      isDataLoading: true,
      cardContainers: "",
      errorState: false,
    };

    this.addCards = this.addCards.bind(this);
  }

  componentDidMount() {
    axios.get("https://fakestoreapi.com/products")
      .then((response) => {
        this.setState(
          {
            myData: response.data,
            isDataLoading: false,
            isFetched: true,
          },
          () => {
            this.addCards(this.state.myData);
          }
        );
      })
      .catch((error) => {
        console.log(error);
        this.setState({
          isDataLoading: false,
          errorState: true,
        });
      });
  }

 
  static getDerivedStateFromProps(props, state) {
    if(props.newProduct) {
      state.myData = props.newProduct
      return state.myData
    } else if(props.updatedProducts) {
      state.myData = props.updatedProducts
      return state.myData
    } else if(props.dataAfterDeletion) {
      state.myData = props.dataAfterDeletion
      return state.myData
    }
    return null
  }

  addCards(data) {
    let cardsArray = [];

    cardsArray = data.map((object) => {
      return (
        <div className="col-12 col-md-3  mb-3" key={object.id}>
          <NavLink to={`/product/${object.id}`}>
            <div className="product-card-container">
              <div>
                <div className="product-image-container">
                  <img className="image-size" src={object.image} alt="" />
                </div>
                {/* Product heading and price container */}
                <div className="product-heading-price-container">
                  <h6 className="product-heading">{object.title}</h6>
                </div>

                {/* Rating and pricing container */}
                <div className="product-rating-price-container">
                  {/* <div>
                    <p className="rating-para">{`Rating: ${object.rating.rate}`}</p>
                  </div> */}
                  <div className="price-container">
                    <i className="fa-solid fa-indian-rupee-sign price-icon"></i>
                    <p className="price-para ">{object.price}</p>
                  </div>
                </div>
                <div>
                  <p className="category-para">{`Category: ${object.category}`}</p>
                </div>
              </div>
            </div>
          </NavLink>
        </div>
      );
    });

    this.setState({
      cardContainers: cardsArray,
    });
  }

  render() {
   
    if(this.state.isDataLoading) {
      return(
        <>
          <div className="spinner-container">
          <div className="text-center">
            <div className="spinner-border" role="status">
              <span className="visually-hidden">Loading...</span>
            </div>
          </div>
        </div>
        </>
      )
    }

    if (this.state.cardContainers) {
      if(Object.keys(this.state.myData).length) {
        return (
          <>
            <div className="container products-main-container">
              <div className="row justify-content-around p-3">
                {this.state.cardContainers}
              </div>
            </div>
          </>
        );
      } else {
        return (
          <>
            <div className="api-unsuccessful">
              <h1>Oops! No Products Found.</h1>
            </div>
          </>
        );
      }

      
    } else if (this.state.errorState) {
      return (
        <>
          <div className="api-unsuccessful">
            <h1>404 Not Found</h1>
            <h4>
              The Page you are requesting is not found. Please try again after
              some time.
            </h4>
          </div>
        </>
      );
    } 
  }
}

export default Products;
