import React from "react";
import {NavLink } from "react-router-dom";
import "./styles.css"

function SignupSuccessPage() {
    return(
        <div>
          <div className={`success-main-container`}>
            <div className="success-child-container">
              <h1>Congrats! Your Account Created Successfully.</h1>
            </div>
            <div className="success-button-containers">
              <NavLink to="/"><button>Home</button></NavLink>
              <NavLink to="/Products"> <button>View Products</button> </NavLink> 
            </div>
          </div>
        </div>
    )
}

export default SignupSuccessPage