import axios from "axios";
import React, { Component } from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import "./App.css";
import Content from "./components/ContentPage";
import Header from "./components/Header";
import Home from "./components/HomePage";
import About from "./components/AboutPage";
import Products from "./components/ProductsPage";
import Product from "./components/ProductIndividual";
import SignupSuccessPage from "./components/SignupSuccessPage";
import CreateProduct from "./components/createProduct";
import UpdateProduct from "./components/UpdateProduct";
import DeleteProduct from "./components/DeleteProduct";

class App extends Component {
  constructor(props) {
    super(props)

    this.state = {
      newProductDetails: "",
      productData: "",
      updatedProductData: "",
      dataAfterDeletion: ""
    }

    this.getNewProductDetails = this.getNewProductDetails.bind(this);
    this.getUpdatedProductDetails = this.getUpdatedProductDetails.bind(this);
    this.deleteProduct = this.deleteProduct.bind(this);
  }

  componentDidMount() {
    axios.get("https://fakestoreapi.com/products")
      .then((response) => response.data)
      .then((data) => {
        this.setState(
          {
            productData : data
          }
          
        );
      })
      .catch((error) => {
        console.log(error);
      });
  }

  getNewProductDetails(data) {
    if(this.state.productData) {
      let newData = this.state.productData.concat(data);
      this.setState({
        newProductDetails: newData
      })
    }
    
  }

  getUpdatedProductDetails(data) {
    let updatedArray = []
    if(this.state.productData) {
      updatedArray = this.state.productData.map((object) => {
        if(object.id === data.id) {
          return data;
        } else {
          return object;
        }
      })
      this.setState({
        updatedProductData: updatedArray
      })
    }
  }

  deleteProduct(data){
    let newArray = []
    if(this.state.productData) {
      newArray = this.state.productData.filter((object) => {
        if(object.id === data.id) {
          return false;
        } else {
          return true;
        }
      })
      
      this.setState({
        dataAfterDeletion : newArray
      })
    }

    
  }


  render() {
    return (
      <BrowserRouter>
        <Header/>
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/About" component={About} />
          <Route exact path="/Products" render={(props) => <Products newProduct={this.state.newProductDetails ? this.state.newProductDetails : null} updatedProducts={this.state.updatedProductData ? this.state.updatedProductData : null} dataAfterDeletion={this.state.dataAfterDeletion ? this.state.dataAfterDeletion : null}  {...props} />}/>
          <Route exact path="/product/:id" render={(props) => <Product {...props} />} />
          <Route exact path="/SignUp" component={Content} />
          <Route exact path="/SignUpSuccess" component={SignupSuccessPage}/> 
          <Route exact path="/CreateProduct" render={(props) => <CreateProduct getNewProductDetails={this.getNewProductDetails} {...props} />} />
          <Route exact path="/UpdateProduct" render={(props) => <UpdateProduct getUpdatedProductDetails={this.getUpdatedProductDetails} {...props} />}/>
          <Route exact path="/DeleteProduct" render={(props) => <DeleteProduct deleteProduct={this.deleteProduct} {...props} />}/>
       </Switch>
      </BrowserRouter>
    );
  }
}

export default App;
