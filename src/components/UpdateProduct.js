import axios from "axios";
import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import validator from "validator";
import "./UpdateProduct.css";

class UpdateProduct extends Component {
  constructor(props) {
    super(props);

    this.state = {
      dataFetched: false,
      myData: "",
      updatedData: "",
      id: "",
      title: "",
      price: "",
      description: "",
      category: "",
      image: "",
      isLoading: false,
      errorOccured: false,
      isFormSubmitted: false,
      isValidId: "",
      isIdValid: "",
      isTitleValid : "",
      isPriceValid: "",
      isDescriptionValid : "",
      isImageUrlValid: "",
      isCategoryValid: ""

    };
    this.handleForm = this.handleForm.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChangeId = this.handleChangeId.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  handleForm(event) {
    event.preventDefault();
    
   if(event.target[0].value) {
    this.setState({
      isLoading: true
    })

    axios.get(`https://fakestoreapi.com/products/${this.state.id}`)
      .then((response) => {
        this.setState({
          myData: response.data,
          id: event.target[0].value,
          isLoading: false
        });
      })
      .catch((error) => {
        console.log(error);
      })

   } else {
     this.setState({
      isValidId: "Please Enter a Valid Id, Enter a number between 1 and 20"
     })
   }

  }

  handleChangeId(event) {
    if((!validator.isDivisibleBy(event.target.value, 1)) ||(Number(event.target.value) > 20) || (Number(event.target.value) < 1)) {
      this.setState({
        isValidId: "Please Enter a Valid Id, Enter a number between 1 and 20"
      })}
       else {
        this.setState({
          isValidId: "",
          id: event.target.value
        })
      }

    }
  

    handleChange(event) {

      if(event.target.id === "id") {
        if((!validator.isDivisibleBy(event.target.value, 1)) ||(Number(event.target.value) > 20) || (Number(event.target.value) < 1)) {
          this.setState({
            isIdValid: "Please Enter a Valid Id, Enter a number between 1 and 20"
          })
        } else {
          this.setState({
            isIdValid: "",
          });
        }
      }else if (event.target.id === "title") {
        if (
          !validator.isAlpha(event.target.value, "en-US", { ignore: " -':;" })
        ) {
          this.setState({
            isTitleValid:
              "Please Enter a Valid Title. It Should contain Letters",
          });
        } else {
          this.setState({
            isTitleValid: "",
            title: event.target.value
          });
        }
      } else if (event.target.id === "price") {
        if (!validator.isDivisibleBy(event.target.value, 1)) {
          this.setState({
            isPriceValid:
              "Please Enter a Valid Price. It should be a Number",
          });
        } else {
          this.setState({
            isPriceValid: "",
            price: event.target.value
          });
        }
      } else if (event.target.id === "description") {
        if (!validator.isAlpha(event.target.value)) {
          this.setState({
            isDescriptionValid:
              "Please Enter a Valid Description. It should contain Letters",
          });
        } else {
          this.setState({
            isDescriptionValid: "",
            description: event.target.value
          });
        }
      } else if (event.target.id === "imageUrl") {
        if (!validator.isURL(event.target.value)) {
          this.setState({
            isImageUrlValid: "Please Enter a Valid Image URL",
          });
        } else {
          this.setState({
            isImageUrlValid: "",
            imageUrl: event.target.value
          });
        }
      } else if (event.target.id === "category") {
        if (!validator.isAlpha(event.target.value, "en-US", { ignore: " -':;" })) {
          this.setState({
            isCategoryValid: "Please Enter a Valid Category",
          });
        } else {
          this.setState({
            isCategoryValid: "",
            category: event.target.value
          });
        }
      }
    }

  handleSubmit(event) {
    event.preventDefault();

    if(!event.target[0].value) {
      this.setState({
        isIdValid: "Please Enter a Valid Id, Enter a number between 1 and 20"
      })
    }

    if(!event.target[1].value) {
      this.setState({
        isTitleValid: "Please Enter a Valid Title. It Should contain Letters"
      })
    }
    if(!event.target[2].value) {
      this.setState({
        isPriceValid:  "Please Enter a Valid Price. It should be a Number"
      })
    }
    if(!event.target[3].value) {
      this.setState({
        isDescriptionValid:
          "Please Enter a Valid Description. It should contain Letters",
      });
    }
    if(!event.target[4].value) {
      this.setState({
        isCategoryValid: "Please Enter a Valid Category",
      });
    }

    if(!event.target[5].value) {
      this.setState({
        isImageUrlValid: "Please Enter a Valid Image URL",
      });
    }
    
    let putObject = {};
    let {id, title, price, description, imageUrl, category} = this.state;
    if(title && price && description && imageUrl && category) {
      this.setState({
        isLoading: true
      })
      putObject['id'] = id;
      putObject['title'] = title;
      putObject['price'] = price;
      putObject['description'] = description;
      putObject['image'] = imageUrl;
      putObject['category'] = category;

    axios.put(`https://fakestoreapi.com/products/${this.state.id}`, putObject)
    .then((response) => response.data)
    .then((data) => {
      console.log(data)
      this.props.getUpdatedProductDetails(data)
      this.setState({
        isLoading: false,
        isFormSubmitted: true
      })
    })
    .catch((error) => {
      console.log(error)
      this.setState({
        errorOccured: true
      })
    })
  }
}

  render() {
    if(this.state.isLoading) {
      return(
        <>
        <div className="spinner-container">
          <div className="text-center">
            <div className="spinner-border" role="status">
              <span className="visually-hidden">Loading...</span>
            </div>
          </div>
        </div>
        </>
      )
    }
    if(this.state.errorOccured) {
      return (
        <>
          <div className="api-unsuccessful">
            <h1>404 Not Found</h1>
            <h4>
              The Page you are requesting is not found. Please try again after
              some time.
            </h4>
          </div>
        </>
      );
    }
    if(this.state.isFormSubmitted) {
      return(
        <>
          <div className="product-update-successful-container">
            <h1>Product Updated Successfully!</h1> 
            <NavLink to="/Products" className="text-center" >
              <button className="btn btn-primary w-50 p-auto">Go To Products</button>
            </NavLink>
          </div>
        </>
      )
    }

    if (this.state.myData) {
      return (
        <div className="d-flex justify-content-center align-items-center id-container">
          <form className="w-50" onSubmit={this.handleSubmit}>
          <div className="mb-3">
              <label className="form-label">id</label>
              <input
                type="text"
                id="id"
                onChange={this.handleChange}
                className="form-control"
              />
              <p className="error-text">{this.state.isIdValid}</p>
            </div>
          <div className="mb-3">
              <label className="form-label">Title</label>
              <input
                type="text"
                id="title"
                onChange={this.handleChange}
                className="form-control"
              />
              <p className="error-text">{this.state.isTitleValid}</p>
            </div>
            <div className="mb-3">
              <label className="form-label">Price</label>
              <input
                type="text"
                id="price"
                onChange={this.handleChange}
                className="form-control"
              />
              <p className="error-text">{this.state.isPriceValid}</p>
            </div>
            <div className="mb-3">
              <label className="form-label">Description</label>
              <input
                type="text"
                id="description"
                onChange={this.handleChange}
                className="form-control"
              />
              <p className="error-text">{this.state.isDescriptionValid}</p>
            </div>
            <div className="mb-3">
              <label className="form-label">Category</label>
              <input
                type="text"
                id="category"
                onChange={this.handleChange}
                className="form-control"
              />
              <p className="error-text">{this.state.isCategoryValid}</p>
            </div>
            <div className="mb-3">
              <label className="form-label">Image</label>
              <input
                type="text"
                id="imageUrl"
                onChange={this.handleChange}
                className="form-control"
              />
              <p className="error-text">{this.state.isImageUrlValid}</p>
            </div>
            <button type="submit" className="btn btn-primary" style={{ width: "50%", height: '10%' }}>
              Update Product
            </button>
          </form>
        </div>
      );
    } else {
      return (
        <>
          <div className="id-container">
            <h2>Enter Id of the Product to Update</h2>
            <form onSubmit={this.handleForm}>
              <input
                type="text"
                onChange={this.handleChangeId}
                style={{ width: "40%", border: "1px solid black" }}
              ></input>
              <p className="error-text">{this.state.isValidId}</p>
              <div>
                <button
                  className="btn btn-primary"
                  style={{ width: "40%", height: '20%' }}
                  type="submit"
                >
                  Get Product Details
                </button>
              </div>
            </form>
          </div>
        </>
      );
    }
  }
}

export default UpdateProduct;
