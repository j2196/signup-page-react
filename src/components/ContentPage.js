import React, { Component } from "react";
import validator from "validator";
import { Redirect } from "react-router-dom";
import "./styles.css";


class Content extends Component {
  constructor(props) {
    super(props);

    this.state = {
      firstName: "",
      lastName: "",
      age: "",
      gender: "",
      role: "",
      email: "",
      password: "",
      repeatPassword: "",
      agreedToTerms: false,

      // Properties for Error messages
      firstNameError: "",
      lastNameError: "",
      ageError: "",
      genderError: "",
      roleError: "",
      emailError: "",
      passwordError: "",
      repeatPasswordError: "",
      agreedToTermsError: "",

      formSubmissionStatus: false,
      passwordEyeslash: "",
      passwordEye: "display-control",
      displayType: "password",
    };

    this.getChangedValue = this.getChangedValue.bind(this);
    this.submitForm = this.submitForm.bind(this);
    this.userErrors = this.userErrors.bind(this);
    this.isSubmittedFormValid = this.isSubmittedFormValid.bind(this);
    this.showPassword = this.showPassword.bind(this);
    this.hidePassword = this.hidePassword.bind(this);
  }

  getChangedValue(event) {
    const { name, value } = event.target;

    if (name === "agreedToTerms") {
      this.setState({
        [name]: event.target.checked,
      });
    } else {
      this.setState({
        [name]: value,
      });
    }
  }

  submitForm(event) {
    event.preventDefault(); //prevents page getting refreshed

    this.userErrors(this.state)
    this.isSubmittedFormValid(this.state) // callback function
    
  }
  
  isSubmittedFormValid(stateObject) {
    let {
      firstName,
      lastName,
      age,
      gender,
      role,
      email,
      password,
      repeatPassword,
      agreedToTerms,
      firstNameError,
      lastNameError,
      ageError,
      genderError,
      roleError,
      emailError,
      passwordError,
      repeatPasswordError,
      agreedToTermsError
    } = stateObject

    if (!(firstNameError && lastNameError && ageError && genderError && roleError && emailError && passwordError && repeatPasswordError && agreedToTermsError)
    && (firstName && lastName && age && gender && role && email && password && repeatPassword && agreedToTerms)) {
      console.log({
        firstName: this.state.firstName,
        lastName: this.state.lastName,
        age: this.state.age,
        gender: this.state.gender,
        role: this.state.role,
        email: this.state.email,
        agreedToTerms: this.state.agreedToTerms

      });
      this.setState({
        formSubmissionStatus: true
      })
    }
  }




  showPassword() {
    this.setState({
      passwordEyeslash: "display-control",
      passwordEye: "",
      displayType: "text"
    })
  }

  hidePassword() {
    this.setState({
      passwordEye: "display-control",
      passwordEyeslash: "",
      displayType: "password"
    })
  }

  userErrors(stateObject) {
    const errors = {};
    
    //destructuring the state object to utilise the properties
    let {
      firstName,
      lastName,
      age,
      gender,
      role,
      email,
      password,
      repeatPassword,
      agreedToTerms,
    } = stateObject
    
    if (!firstName) {
      errors.firstNameError = "Enter First Name.";
    } else if (!validator.isAlpha(firstName.trim())) {
      errors.firstNameError =
        "Please Enter a Valid Name. First Name Should Contain Only Letters.";
    } else if(firstName.length < 2) {
      errors.firstNameError =
        "Please Enter a Valid Name";
    }
    else {
        errors.firstName = firstName.trim()
        errors.firstNameError = ""
    }

    if (!lastName) {
      errors.lastNameError = "Enter Last Name.";
    } else if (!validator.isAlpha(lastName.trim())) {
      errors.lastNameError =
        "Please Enter a Valid Name. Last Name Should Contain Only Letters.";
    } else if(lastName.length < 2) {
      errors.lastNameError =
        "Please Enter a Valid Name";
    }
    else {
        errors.lastName = lastName.trim()
        errors.lastNameError = "";
    }


    if (!age) {
      errors.ageError = "Enter Date of Birth.";
    } else if(!validator.isDate(age)) {
      errors.ageError = "Please Enter a Valid Date"
    } else {
      errors.ageError = "";
    }

    if (!gender) {
      errors.genderError = "Please Select Gender.";
    } else {
      errors.genderError = ""
    }

    if (!role) {
      errors.roleError = "Please Select Role.";
    } else {
      errors.roleError = "";
    }

    if (!email) {
      errors.emailError = "Enter Email Address.";
    } else if (!validator.isEmail(email)) {
      errors.emailError = "Please Enter a Valid Email Address.";
    } else {
      errors.emailError = "";
    }

    if (!password) {
      errors.passwordError = "Enter Password.";
    } else if (!validator.isStrongPassword(password)) {
      errors.passwordError = "Please Enter the Password as per the Guidelines.";
    } else {
      errors.passwordError = "";
    }

    if (!repeatPassword) {
      errors.repeatPasswordError = "Re-enter Password";
    } else if (password !== repeatPassword) {
      errors.repeatPasswordError = "Passwords did not match. Please Try Again.";
    } else {
      errors.repeatPasswordError = "";
    }

    if (!agreedToTerms) {
      errors.agreedToTermsError = "Please Agree to the Terms&Conditions";
    } else {
      errors.agreedToTermsError = "";
    }

    this.setState(errors);

    return null;
  }

  render() {

    let {formSubmissionStatus} = this.state

    let contentjsx =  <div>
    <div>
      <div className={`main-container `}>
        <div className="form-container">
          <form onSubmit={this.submitForm}  noValidate="noValidate" className="formContainer">
            <div>
              <h1 className="signup-heading">Sign Up to <span>West Side</span></h1>
              <p>
                Already have an account? <span className="signin-link">Sign In</span>
              </p>
            </div>
            {/* container 1 */}
            <div className="names-container">
              {/* First Name input */}
              <div className="individual-container">
                <div className="inside-container">
                  <i className="fa-solid fa-circle-user"></i>
                  <input
                    type="text"
                    name="firstName"
                    value={this.state.firstName}
                    placeholder="First Name"
                    className="selected-input-styling"
                    onChange={this.getChangedValue}
                  />
                </div>
                <p className="errors-para">{this.state.firstNameError}</p>
                <hr />
              </div>
              {/* Last Name Input */}
              <div className="individual-container">
                <div className="inside-container">
                  <i className="fa-solid fa-circle-user"></i>
                  <input
                    type="text"
                    name="lastName"
                    value={this.state.lastName}
                    placeholder="Last Name"
                    className="selected-input-styling"
                    onChange={this.getChangedValue}
                  />
                </div>
                <p className="errors-para">{this.state.lastNameError}</p>
                <hr />
              </div>
            </div>
            {/* container 2 */}
            <div className="dob-gender-container">
              {/* Date of birth */}
              <div className="individual-container">
                <div className="inside-container">
                  <i className="fa-solid fa-cake-candles"></i>
                  <input
                    type="date"
                    name="age"
                    
                    value={this.state.age}
                    placeholder="Date of Birth"
                    className="selected-input-styling"
                    onChange={this.getChangedValue}
                  />
                </div>
                <p className="errors-para">{this.state.ageError}</p>
                <hr />
              </div>
              {/* Gender */}
              <div className="individual-container">
                <div className="inside-container inside-container-gender">
                  <i className="fa-solid fa-venus-mars"></i>
                  <select
                    name="gender"
                    value={this.state.gender}
                    onChange={this.getChangedValue}
                    className="selected-input-styling select-styling"
                    
                  >
                    <option value="">Gender</option>
                    <option>Male</option>
                    <option>Female</option>
                    <option>Prefer Not to Answer</option>
                  </select>
                </div>
                <p className="errors-para">{this.state.genderError}</p>
                <hr />
              </div>
            </div>
            {/* container 3 */}
            <div className="role-email-container">
              {/* Role */}
              <div className="individual-container">
                <div className="inside-container">
                  <i className="fa-brands fa-black-tie"></i>
                  <select
                    name="role"
                    value={this.state.role}
                    onChange={this.getChangedValue}
                    className="selected-input-styling select-styling"
          
                  >
                    <option value="">Role</option>
                    <option>CTO</option>
                    <option>Lead Engineer</option>
                    <option>Senior Engineer</option>
                    <option>Developer</option>
                  </select>
                </div>
                <p className="errors-para">{this.state.roleError}</p>
                <hr />
              </div>

              {/* Email */}
              <div className="individual-container">
                <div className="inside-container">
                  <i className="fa-solid fa-envelope"></i>
                  <input
                    type="text"
                    name="email"
                    value={this.state.email}
                    placeholder="Email Address"
                    className="selected-input-styling"
                    onChange={this.getChangedValue}
                  />
                </div>
                <p className="errors-para">{this.state.emailError}</p>
                <hr />
              </div>
            </div>

            {/* container 4 */}
            <div className="passwords-container">
              {/* password */}
              <div className="individual-container">
                <div className="inside-container">
                  <i className="fa-solid fa-key"></i>
                  <input
                    type={`${this.state.displayType}`}
                    name="password"
                    value={this.state.password}
                    placeholder="Password"
                    className="selected-input-styling-password "
                    onChange={this.getChangedValue}
                  />
                  <i className={`fa-solid fa-eye-slash password-show-and-hide-icon ${this.state.passwordEyeslash}`} onClick={this.showPassword}></i>
                  <i className={`fa-solid fa-eye password-show-and-hide-icon ${this.state.passwordEye}`} onClick={this.hidePassword}></i>
                </div>
                <p className="errors-para">{this.state.passwordError}</p>
                <hr />
              </div>
              {/* retype password */}
              <div className="individual-container">
                <div className="inside-container">
                  <i className="fa-solid fa-rotate-right"></i>
                  <input
                    type="password"
                    name="repeatPassword"
                    value={this.state.repeatPassword}
                    placeholder="Repeat Password"
                    className="selected-input-styling"
                    onChange={this.getChangedValue}
                  />
                </div>
                <p className="errors-para">
                  {this.state.repeatPasswordError}
                </p>
                <hr />
              </div>
            </div>

            {/* Container 5 */}
            <div className="password-guidelines-container">
              <h3 className="password-guide-heading">
                Password Guidelines
              </h3>
              <p className="password-guide-para">
                Minimum Characters length - 8. <br/>
                Atleast 1 Lowercase Letter, Uppercase Letter and Number. <br/>
                Atleast 1 Special Character.
              </p>
            </div>

            {/* container 6 */}
            <div className="terms-container">
              {/* checkbox */}
              <input
                type="checkbox"
                name="agreedToTerms"
                value={this.state.agreedToTerms}
                onChange={this.getChangedValue}
                id="checkBox"
              />
              <label htmlFor="checkBox">
                I agree to the Terms&Conditions
              </label>
              <p className="errors-para">
                {this.state.agreedToTermsError}
              </p>
            </div>

            {/*Container7 & Submit */}
            <div className="submit-btn">
              <button>Submit</button>
            </div>
          </form>
        </div>
      </div>
    </div>
    </div>
    let returnValue = formSubmissionStatus ? <Redirect  to="/SignUpSuccess" /> : contentjsx

    return (
     <>
      {returnValue}
     </>
    );
  }
}

export default Content;
