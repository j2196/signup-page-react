import axios from "axios";
import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import validator from "validator";
import "./CreateProducts.css";

class CreateProduct extends Component {
  constructor(props) {
    super(props);

    this.state = {
      dataToBeCreated: "",
      isFormSubmitted: false,
      title: "",
      price: "",
      description: "",
      imageUrl: "",
      category: "",
      isTitleValid: "",
      isPriceValid: "",
      isDescriptionValid: "",
      isImageUrlValid: "",
      isCategoryValid: "",
      productCreationLoading: false,
    };
    this.handleForm = this.handleForm.bind(this);
    this.callData = this.callData.bind(this);
    this.handleChange = this.handleChange.bind(this);
    // console.log(this.props)
  }

  callData(data) {
    axios
      .post("https://fakestoreapi.com/products", data)
      .then((response) => response.data)
      .then((data) => {
        this.props.getNewProductDetails(data);
        this.setState({
          productCreationLoading: false,
          isFormSubmitted: true
        })
      })
      .catch((error) => {
        console.log(error);
      });
  }

  handleForm(event) {
    event.preventDefault();

    if(!event.target[0].value) {
      this.setState({
        isTitleValid: "Please Enter a Valid Title. It Should contain Letters"
      })
    }
    if(!event.target[1].value) {
      this.setState({
        isPriceValid:  "Please Enter a Valid Price. It should be a Number"
      })
    }
    if(!event.target[2].value) {
      this.setState({
        isDescriptionValid:
          "Please Enter a Valid Description. It should contain Letters",
      });
    }
    if(!event.target[3].value) {
      this.setState({
        isImageUrlValid: "Please Enter a Valid Image URL",
      });
    }
    if(!event.target[4].value) {
      this.setState({
        isCategoryValid: "Please Enter a Valid Category",
      });
    }

    let postObject = {};

    let {title, price, description, imageUrl, category} = this.state;

    if(title && price && description && imageUrl && category) {
      postObject['title'] = title;
      postObject['price'] = price;
      postObject['description'] = description;
      postObject['image'] = imageUrl;
      postObject['category'] = category;

      this.setState({
        productCreationLoading: true
      }, ()=> {
        this.callData(postObject)
      })
      
    }
    
  }

  handleChange(event) {
    if (event.target.id === "title") {
      if (
        !validator.isAlpha(event.target.value, "en-US", { ignore: " -':;" })
      ) {
        this.setState({
          isTitleValid:
            "Please Enter a Valid Title. It Should contain Letters",
        });
      } else {
        this.setState({
          isTitleValid: "",
          title: event.target.value
        });
      }
    } else if (event.target.id === "price") {
      if (!validator.isDivisibleBy(event.target.value, 1)) {
        this.setState({
          isPriceValid:
            "Please Enter a Valid Price. It should be a Number",
        });
      } else {
        this.setState({
          isPriceValid: "",
          price: event.target.value
        });
      }
    } else if (event.target.id === "description") {
      if (!validator.isAlpha(event.target.value)) {
        this.setState({
          isDescriptionValid:
            "Please Enter a Valid Description. It should contain Letters",
        });
      } else {
        this.setState({
          isDescriptionValid: "",
          description: event.target.value
        });
      }
    } else if (event.target.id === "imageUrl") {
      if (!validator.isURL(event.target.value)) {
        this.setState({
          isImageUrlValid: "Please Enter a Valid Image URL",
        });
      } else {
        this.setState({
          isImageUrlValid: "",
          imageUrl: event.target.value
        });
      }
    } else if (event.target.id === "category") {
      if (!validator.isAlpha(event.target.value, "en-US", { ignore: " -':;" })) {
        this.setState({
          isCategoryValid: "Please Enter a Valid Category",
        });
      } else {
        this.setState({
          isCategoryValid: "",
          category: event.target.value
        });
      }
    }
  }

  render() {
    if (this.state.productCreationLoading) {
      return (
        <>
          <div className="spinner-container">
            <div className="text-center">
              <div className="spinner-border" role="status">
                <span className="visually-hidden">Loading...</span>
              </div>
            </div>
          </div>
        </>
      );
    }
    if (this.state.isFormSubmitted) {
      return (
        <>
          <div className="product-update-successful-container">
            <h1>Product Created Successfully!</h1>
            <NavLink to="/Products" className="text-center">
              <button className="btn btn-primary w-50 h-100">
                Go To Products
              </button>
            </NavLink>
          </div>
        </>
      );
    } else {
      return (
        <div className="d-flex justify-content-center align-items-center form-create">
          <form className="w-50" onSubmit={this.handleForm}>
            <div className="mb-3">
              <label className="form-label">Title</label>
              <input
                type="text"
                id="title"
                onChange={this.handleChange}
                className="form-control"
              />
              <p className="error-text">{this.state.isTitleValid}</p>
            </div>
            <div className="mb-3">
              <label className="form-label">Price</label>
              <input
                type="text"
                id="price"
                onChange={this.handleChange}
                className="form-control"
              />
              <p className="error-text">{this.state.isPriceValid}</p>
            </div>
            <div className="mb-3">
              <label className="form-label">Description</label>
              <input
                type="text"
                id="description"
                onChange={this.handleChange}
                className="form-control"
              />
              <p className="error-text">{this.state.isDescriptionValid}</p>
            </div>
            <div className="mb-3">
              <label className="form-label">Image</label>
              <input
                type="text"
                id="imageUrl"
                onChange={this.handleChange}
                className="form-control"
              />
              <p className="error-text">{this.state.isImageUrlValid}</p>
            </div>
            <div className="mb-3">
              <label className="form-label">Category</label>
              <input
                type="text"
                id="category"
                onChange={this.handleChange}
                className="form-control"
              />
              <p className="error-text">{this.state.isCategoryValid}</p>
            </div>

            <button type="submit" className="btn btn-primary w-100 h-25">
              Create Product
            </button>
          </form>
        </div>
      );
    }
  }
}

export default CreateProduct;
